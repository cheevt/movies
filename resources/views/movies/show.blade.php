@extends('layouts.master')
@section('title')
{{ $movie->title }}
@endsection
@section('content')
<div class="col-sm-8 blog-main">
    <h2>{{ $movie->title }}</h2>
    <p>Genre: <a href='{{ route('single-genre', ['genre' => $movie->genre]) }}'>{{ $movie->genre }}</a></p>
    <p>Director: {{ $movie->director }}</p>
    <p>Year: {{ date('Y', strtotime($movie->year)) }}</p>
    <p>Storyline: <br/><hr> {{ $movie->storyline }}</p>

    <h4>Comments</h4>
    <ul class="list-unstyled">
        @forelse($movie->comments as $comment)
        <li>{{ $comment->created_at }} | {{ $comment->content }}</li>
        @empty
        <li>No comments!</li>
        @endforelse
    </ul>

    <h4>Add comment</h4>
    <form method="POST" action="{{ route('add-comment', ['movie_id' => $movie->id]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="content">Text</label>
            <textarea id="content" rows="5" name="content" class="form-control"></textarea>
            @include('partials.error-message', ['fieldTitle' => 'content'])
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger">Add new</button>
        </div>
    </form>
</div>
@endsection