@extends('layouts.master')
@section('title')
Movies
@endsection

@section('content')
<div class="col-sm-8 blog-main">
    <h2>Movies</h2>


    @foreach($movies as $movie)
    <div>
        <a href="{{ route('single-movie', ['id' => $movie->id]) }}"><h3>{{ $movie->title }}</h3></a>
        <p>{{ str_limit($movie->storyline, 100) }}</p>
    </div>
    @endforeach
</div>
@endsection