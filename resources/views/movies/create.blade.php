@extends('layouts.master')
@section('title')
Add new movie
@endsection

@section('content')
<div class="col-sm-8 blog-main">
    <h2>Add new movie</h2>
    <form method="POST" action="{{ route('store-movie') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" id="title" name="title" class="form-control" />
            @include('partials.error-message', ['fieldTitle' => 'title'])
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" id="genre" name="genre" class="form-control" />
            @include('partials.error-message', ['fieldTitle' => 'genre'])
        </div>
        <div class="form-group">
            <label for="director">Director</label>
            <input type="text" id="director" name="director" class="form-control" />
            @include('partials.error-message', ['fieldTitle' => 'director'])
        </div>
        <div class="form-group">
            <label for="year">Year</label>
            <input type="date" id="year" name="year" class="form-control" />
            @include('partials.error-message', ['fieldTitle' => 'year'])
        </div>
        <div class="form-group">
            <label for="storyline">Storyline</label>
            <textarea id="storyline" rows="10" name="storyline" class="form-control"></textarea>
            @include('partials.error-message', ['fieldTitle' => 'storyline'])
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger">Add new</button>
        </div>
    </form>
</div>
@endsection