@include('partials.header')

<div class="container">

    <div class="row">
        @yield('content')


        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            @include('partials.sidebar')
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->

@include('partials.footer')