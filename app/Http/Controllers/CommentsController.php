<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Movie;

class CommentsController extends Controller {

    public function store(Request $request, $movieId) {
        $movie = Movie::find($movieId);
        //dd($movie);

        $this->validate($request, [
            'content' => 'required|min:15'
        ]);

        $movie->comments()->create($request->all());

        return redirect()->back();
    }

}
